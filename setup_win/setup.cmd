:: Setup virtualenv for this project

set SETUP_DIR=%~dp0
set PROJECT_DIR=%SETUP_DIR%..\

:: Change to project dir
cd %PROJECT_DIR%

:: Install python before
:: Check activate.bat file
:: Install virtualenv if not exist else pass
:: if not exist %PROJECT_DIR%.venv\Scripts\activate.bat (
:: Install virtualenv from installed python
:: virtualenv .venv
:: )

:: FOR /F  %%i IN ('where python') DO set PYTHON_BIN=%%i
:: F
:: if not exist %PYTHON_BIN% ( 
:: echo "Please, install python 2.7"
:: exit
:: }

:: set TCL_LIBRARY=%PYTHON%\tcl\tcl8.5
:: set TK_LIBRARY=%PYTHON%\tcl\tk8.5
:: set TIX_LIBRARY

:: if not exist %PROJECT_DIR%.venv\Lib\tcl8.5 (
::    mklink /d %PROJECT_DIR%env\Lib\tcl8.5 %PYTHON%\tcl\tcl8.5
:: )

:: if not exist %PROJECT_DIR%.venv\Lib\tk8.5 (
::    mklink /d %PROJECT_DIR%env\Lib\tk8.5 %PYTHON%\tcl\tk8.5
:: )



:: Load virtualenv in .venv directory .venv\Scripts\activate.bat
:: call .venv\Scripts\activate.bat

:: Install requirements
pip install -r requirements.txt

:: start /b cmd

:: Install matplotlib
:: pip install matplotlib

:: Install cson
:: pip install python-cson
pause
