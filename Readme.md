ADB sniffer

## Requirement

- adb 
- python 
- matplotlib

## Install

### Checkout code
git clone https://gitlab.com/duydb2/adb_sniffer -b master

### Run install script
- For window: setup_win\setup.cmd
- For mac: setup_mac\setup.sh
- For linux: setup_linux\setup.sh
  + Fedora: pip install --upgrade --ignore-installed matplotlib[mplot3d]

## Usage
