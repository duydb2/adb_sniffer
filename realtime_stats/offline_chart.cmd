@echo off

:: Setup virtualenv for this project

set SCRIPTS_DIR=%~dp0
set PROJECT_DIR=%SCRIPTS_DIR%..\
:: Change to project dir

set PATH=J:\d_backup\tools\python2;%PATH%

set PYTHONPATH=%~dp0src;%PYTHONPATH%


:: Defer control.

"python" "%~dp0offline_chart.py" %*
pause
