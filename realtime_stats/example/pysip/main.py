import sys
import os

script_directory_path = os.path.dirname(sys.argv[0])
module_directory_path = script_directory_path + '\\src'
# environment setup
# if os.environ.has_key('PYTHONPATH'):
#   os.environ['PYTHONPATH'] = module_directory_path + \
#       ';' if os.name == nt else ':' + os.environ['PYTHONPATH']
# else:
#   os.environ['PYTHONPATH'] = module_directory_path
# if not module_directory_path in sys.path:
#   sys.path.append(module_directory_path)

# import pysip

# os.system('python -m unittest discover -v pysip')
path = os.path.abspath(os.path.split(__file__)[0])
if __name__ == "__main__":
  # os.system('cd scripts && find . -name *.pyc -exec rm -f {} +')
  execfile(os.path.join(path, 'gyp_chromium'))
  execfile(os.path.join(path, 'gyp_webrtc'))
