import numpy as np
import wx
from wx.py.shell import Shell

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
import matplotlib.pyplot as plt


class ShellPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.shell = Shell(self)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.shell, 1, wx.GROW)
        self.SetSizer(self.sizer)
        self.Layout()
        self.Fit()


class FigurePanel(wx.Panel):
    def __init__(self, parent):

        wx.Panel.__init__(self, parent)
        self.parent = parent
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.figure, ax = plt.subplots()
        self.canvas = FigureCanvas(self, -1, self.figure)
        self.shellpanel = ShellPanel(self)
        s1 = wx.BoxSizer(wx.VERTICAL)
        s1.Add(self.canvas, 0, wx.GROW)
        s1.Add(self.shellpanel, 1 , wx.EXPAND)
        self.sizer.Add(s1, 5, wx.GROW)

        self.SetSizer(self.sizer)
        self.Layout()
        self.Fit()

    def LoadFigure(self):
        self.figure, ax = plt.subplots(2, 2)
        self.canvas.draw()
        self.Refresh()


class FigureFrame(wx.Frame):
    def __init__(self, parent, id, title, size):
        wx.Frame.__init__(self, parent, id, title, size=size)

if __name__ == "__main__":
    app = wx.App(False)
    fr = FigureFrame(None, -1, title='Figure Loader', size=(wx.DisplaySize()[0]/2, wx.DisplaySize()[1]*3/4))
    panel = FigurePanel(fr)
    fr.Show()
    app.MainLoop()