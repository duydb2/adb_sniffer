#!/usr/bin/env python

from __future__ import division
import os
import sys

import subprocess
import hashlib
import matplotlib.pyplot as plt
import gzip
import math

local_path = os.path.abspath(os.path.split(__file__)[0])
tmp_path = os.path.join(local_path,"tmp")
if not os.path.exists(tmp_path):
    os.makedirs(tmp_path)
data_dirs = []
data_files = []
rs_files = []

# data_dirs.append(local_path)
data_dirs.append('/home/duydb2/proj/zalo/stats_log_data/23_12_2015')
# data_dirs.append('/home/duydb2/proj/zalo/stats_log_data/24_12_2015')
# data_dirs.append('/home/duydb2/proj/zalo/stats_log_data/26_12_2015')

bwe_executable = os.path.abspath(os.path.join(local_path,os.path.pardir,os.path.pardir,"out","build","jitter-est","zalo_test", "test", "bwe-test"))

def get_out_path(path):
    paths = path.split(os.sep)
    dir = paths[-2]
    name= paths[-1]
    print dir,name
    outfile= os.path.join(local_path, "tmp", dir + "_" + name + "_result")
    outpicfile = os.path.join(local_path, "tmp", dir + "_" + name +".out" +".png")
    inpicfile = os.path.join(local_path, "tmp",  dir + "_" + name + ".in" +".png")
    return outfile, outpicfile, inpicfile

def get_data_files(dirs, exten = '.csv'):
    rs = {}
    for topdir in dirs:
        for root, subdirs, files in os.walk(topdir):
            for name in files:
                if name.lower().endswith(exten):
                    path = os.path.join(root,name)
                    file_hash = hashfile(path)
                    # Add or append the file path
                    if file_hash in rs:
                        rs[file_hash].append(path)
                    else:
                        rs[file_hash] = [path]
    return rs

def map_call_id(files):
    rs = {}
    count = 0
    for item in files:
        basename, exten = os.path.splitext(os.path.basename(item))
        id = basename.split('_')[2]
        rs.setdefault(id,[]).append(item)
        # if len(rs.get(id)) > 1:
        #         rs.update({id:temp[id]})
    
    # print count
    # count = 0
    # for k,v in rs.items():
    #     if len(v) > 1:
    #         count +=1 
    #         print k,v
    # 
    # print len(rs), count
    return rs
    
def hashfile(path, blocksize = 65536):
    afile = open(path, 'rb')
    hasher = hashlib.md5()
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    afile.close()
    return hasher.hexdigest()

def write_to_file(data,file):
    print "data write to %s" % file
    with open(file,'w') as f:
        f.write(data)

def main2(argv):
    import graph
    data_files = [ file[0] for file in get_data_files(data_dirs).values()]
    for infile in data_files:
        print ">>>  %s" % infile
        outfile, outpicfile, inpicfile = get_out_path(infile)
        rs = subprocess.Popen([bwe_executable,infile], stdout=subprocess.PIPE).communicate()[0]
        write_to_file(rs, outfile)
        rs_files.append(outfile)
        # check result
        # draw graph
        # graph.draw_png_from_file(infile,inpicfile)
        graph.draw_png_from_file(outfile,outpicfile)

def show1(path):
    import logcat.offline_chart2 as chart
    plt1,fig1 = chart.plot(path,  plt.figure(figsize=(22,60),dpi=75))
    plt1.show()
    
def show2(path1, path2):
    import logcat.offline_chart2 as chart
    plt1,fig1 = chart.plot(path1,  plt.figure(figsize=(22,60),dpi=75))
    plt2,fig2 = chart.plot(path2,  plt.figure(figsize=(22,60),dpi=75))
    plt1.show()
    plt2.show()
        
def show(call_ids, id = None):
    if id:
        call = call_ids[id]
        print call
        if len(call) > 1:
            show2(call[0], call[1])
        else:
            show1(call[0])
           
    else:
        for id in call_ids:
            call = call_ids[id]
            print call
            if len(call) > 1:
                show2(call[0], call[1])
            else:
                show1(call[0])       
    
def main(argv):
    data_files = [file[0] for file in get_data_files(data_dirs,'.gz').values()]
    call_ids = map_call_id(data_files)
    show(call_ids, '1065315590')
    
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))