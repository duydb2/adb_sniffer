#!/usr/bin/env python

from __future__ import division
import os
import sys

import subprocess
import hashlib
import matplotlib.pyplot as plt
import gzip
import math
import re

local_path = os.path.abspath(os.path.split(__file__)[0])

report_file = os.path.join(local_path,'congest.txt')

call_sessions = []
points = []

def load_report(path):
    print path
    with open(path,'r') as file:
        for line in file:
            rs = re.compile(r'^\d+.*,.*\d+.*\n$', re.IGNORECASE | re.MULTILINE).search(line)
            if rs:
                rs = rs.group(0)
                element =  rs.split(',')
                s = element[2].strip()
                points.append(float(s) if s else -1)
                call_sessions.append(element[1])
        
    print [i < 4 and i > 0 for i in points].count(True), len(call_sessions), [i < 4 and i > 0 for i in points].count(True)/len(call_sessions) * 100 
    for i,v in enumerate(points):
        if v < 4 and v > 0:
            print call_sessions[i], v

def main(argv):
    load_report(report_file)
    
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))