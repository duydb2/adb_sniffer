from unqlite import UnQLite

db = UnQLite()  # Create an in-memory database.
db['foo'] = 'bar'  # Use as a key/value store.
print db['foo']

for i in range(4):
  db['k%s' % i] = str(i)

'k3' in db

raw_input(prompt=true)

'k4' in db

del db['k3']

db.append('k2', 'XXXX')

db['k2']

[item for item in db]
