import pcap
import dpkt
import time
import binascii
import pcap
import sys
import string
import time
import socket
import struct

""" Reference:
 - http://3gfp.com/wp/2015/03/sniffing-tcp-packets-with-python/
 - http://3gfp.com/wp/2015/03/sniffing-tcp-packets-with-python/
 - [Strange raw socket on mac osx](http://stackoverflow.com/questions/6878603/strange-raw-socket-on-mac-os-x)
 - https://code.google.com/p/winpcapy/
 - http://pylibpcap.sourceforge.net/
 - https://github.com/DanMcInerney/net-creds
"""

DEBUG = False

def print_hex_string_nicely(hex_string):
    index = 0
    result = ''
    while hex_string:
        result += '{:08x}: '.format(index)
        index += 16
        line, hex_string = hex_string[:32], hex_string[32:]
        while line:
            two_bytes, line = line[:4], line[4:]
            if two_bytes:
                result += two_bytes + ' '
        result = result[:-1] + '\n'
    print result

def hex_dump_packet(packet_data):
    print_hex_string_nicely(binascii.hexlify(packet_data))

packet_function = None

def get_tcp_from_ethernet(data):
    packet = dpkt.ethernet.Ethernet(data)
    if isinstance(packet.data, dpkt.ip.IP):
        return packet.data.data.data
    return None

def get_tcp_from_loopback(data):
    packet = dpkt.loopback.Loopback(data)
    if isinstance(packet.data, dpkt.ip.IP):
        return packet.data.data.data
    return None

def get_tcp_from_ip(data):
    packet = dpkt.ip.IP(data)
    if isinstance(packet, dpkt.ip.IP):
        return packet.data.data
    return None

def determine_packet_function(packet_data):
    type_functions = [get_tcp_from_ethernet, get_tcp_from_loopback, get_tcp_from_ip]
    for fn in type_functions:
        if fn(packet_data) is not None:
            if DEBUG: print 'Packet type:', fn.__name__.split('_')[-1]
            return fn
    return None

def tcp_data_from_packet_data(packet_data):
    global packet_function
    if not packet_function:
        packet_function = determine_packet_function(packet_data)
        if not packet_function:
            return None
    return packet_function(packet_data)


def tcp_data_from_filter(filter="", interface=None):
    # interface notes:
    #     iptap and pktap alone act like ",any" is appended
    #     'any' is a synonym for 'pktap,any'
    #     pktap and iptap do not work with permiscuous mode
    #     iptap seems to take no more than 23 characters
    #     pktap only takes 8 interfaces
    #     pcap.findalldevs() will return a list of interfaces
    #     Using iptap makes coding easier since pcap will only
    #     return the IP portion of the packet
    if not interface:
        interface="iptap"
    if DEBUG: print 'Capturing on interface(s):',interface
    # You must set timeout_ms. Not sure why the default doesn't work.
    pc = pcap.pcap(name=interface,           # default: None
                   snaplen=256 * 1024,       # default: 64k, but tcpdump uses 256k
                   timeout_ms=500)           # defailt: 500, but tcpdump uses 1000
    pc.setfilter(filter)
    for capture in pc:
        if not capture:
            continue
        timestamp, packet_data = capture
        if DEBUG: hex_dump_packet(packet_data)
        tcp_data = tcp_data_from_packet_data(packet_data)
        if tcp_data is not None:
            yield timestamp, tcp_data

def timestring(timestamp):
    # 00:14:21.836925
    t = time.localtime(timestamp)
    s = '{:0.6f}'.format(timestamp - int(timestamp))[1:]
    return '{:02}:{:02}:{:02}{}'.format(t.tm_hour, t.tm_min, t.tm_sec, s)

def print_tcp_data_from_filter(**kwargs):
    for timestamp, data in tcp_data_from_filter(**kwargs):
        print "{}    {}".format(timestring(timestamp), data)

# Only show packets containing actual data, i.e. no protocol-only
# packets, coming to my server on port 777.
filter = 'tcp dst port 777 and (tcp[tcpflags] & tcp-push != 0)'
print_tcp_data_from_filter(filter=filter)


protocols={socket.IPPROTO_TCP:'tcp',
           socket.IPPROTO_UDP:'udp',
           socket.IPPROTO_ICMP:'icmp'}

def decode_ip_packet(s):
  d={}
  d['version']=(ord(s[0]) & 0xf0) >> 4
  d['header_len']=ord(s[0]) & 0x0f
  d['tos']=ord(s[1])
  d['total_len']=socket.ntohs(struct.unpack('H',s[2:4])[0])
  d['id']=socket.ntohs(struct.unpack('H',s[4:6])[0])
  d['flags']=(ord(s[6]) & 0xe0) >> 5
  d['fragment_offset']=socket.ntohs(struct.unpack('H',s[6:8])[0] & 0x1f)
  d['ttl']=ord(s[8])
  d['protocol']=ord(s[9])
  d['checksum']=socket.ntohs(struct.unpack('H',s[10:12])[0])
  d['source_address']=pcap.ntoa(struct.unpack('i',s[12:16])[0])
  d['destination_address']=pcap.ntoa(struct.unpack('i',s[16:20])[0])
  if d['header_len']>5:
    d['options']=s[20:4*(d['header_len']-5)]
  else:
    d['options']=None
  d['data']=s[4*d['header_len']:]
  return d


def dumphex(s):
  bytes = map(lambda x: '%.2x' % x, map(ord, s))
  for i in xrange(0,len(bytes)/16):
    print '    %s' % string.join(bytes[i*16:(i+1)*16],' ')
  print '    %s' % string.join(bytes[(i+1)*16:],' ')
    

def print_packet(pktlen, data, timestamp):
  if not data:
    return

  if data[12:14]=='\x08\x00':
    decoded=decode_ip_packet(data[14:])
    print '\n%s.%f %s > %s' % (time.strftime('%H:%M',
                                           time.localtime(timestamp)),
                             timestamp % 60,
                             decoded['source_address'],
                             decoded['destination_address'])
    for key in ['version', 'header_len', 'tos', 'total_len', 'id',
                'flags', 'fragment_offset', 'ttl']:
      print '  %s: %d' % (key, decoded[key])
    print '  protocol: %s' % protocols[decoded['protocol']]
    print '  header checksum: %d' % decoded['checksum']
    print '  data:'
    dumphex(decoded['data'])
 

if __name__=='__main__':

  if len(sys.argv) < 3:
    print 'usage: sniff.py <interface> <expr>'
    sys.exit(0)
  p = pcap.pcapObject()
  #dev = pcap.lookupdev()
  dev = sys.argv[1]
  net, mask = pcap.lookupnet(dev)
  # note:  to_ms does nothing on linux
  p.open_live(dev, 1600, 0, 100)
  #p.dump_open('dumpfile')
  p.setfilter(string.join(sys.argv[2:],' '), 0, 0)

  # try-except block to catch keyboard interrupt.  Failure to shut
  # down cleanly can result in the interface not being taken out of promisc.
  # mode
  #p.setnonblock(1)
  try:
    while 1:
      p.dispatch(1, print_packet)

    # specify 'None' to dump to dumpfile, assuming you have called
    # the dump_open method
    #  p.dispatch(0, None)

    # the loop method is another way of doing things
    #  p.loop(1, print_packet)

    # as is the next() method
    # p.next() returns a (pktlen, data, timestamp) tuple 
    #  apply(print_packet,p.next())
  except KeyboardInterrupt:
    print '%s' % sys.exc_type
    print 'shutting down'
    print '%d packets received, %d packets dropped, %d packets dropped by interface' % p.stats()
