import os
import sys

local_path = os.path.dirname(os.path.realpath(__file__))

if __name__ == "__main__":
  execfile(os.path.join(local_path, 'app_cleanup'))

  def print_match(items):
    if items == []:
        return
    print items
  depth_clean(path=os.path.join(local_path, os.pardir, os.pardir, os.pardir),
              f=print_match,
              pattern='*logcat\\*'
              )
