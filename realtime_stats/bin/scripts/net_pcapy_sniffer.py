#!/usr/bin/env python
""" Reference:
 - https://breakingcode.wordpress.com/2010/04/02/using-impacketpcapy-with-python-2-6-on-windows/
 - https://github.com/OpenRCE/sulley/wiki/Windows-Installation
 - http://snipplr.com/view/3579/live-packet-capture-in-python-with-pcapy/
 - http://www.binarytides.com/code-a-packet-sniffer-in-python-with-pcapy-extension/
 - http://stackoverflow.com/questions/21698894/scapy-error-no-module-names-pcapy
 - http://www.secdev.org/projects/scapy/doc/installation.html
 - https://docs.google.com/viewer?url=http%3A%2F%2Fwww.winpcap.org%2Fdocs%2FWinPcap-SBAC03.pdf
"""

