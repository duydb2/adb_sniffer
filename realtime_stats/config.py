data_dir_path = '/Users/cpu11711-local/Downloads/'
data_file_path = data_dir_path + '114614964_178882098_1465869850_1465869835506_-1.gz'
logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-10s) %(message)s')

call_log_file = 'D:/vm/webrtc-pjproject/adb_sniffer/realtime_stats/call2.txt'
log_file = None

font = {'family': 'serif',
        'color': 'darkred',
        'weight': 'normal',
        'size': 16,
        }

fontP = FontProperties()
fontP.set_size('x-small')

id_audio = 0
id_video = 3
plots = {
    20: [id_audio,
         ["%ds", "%dp"],
         [['[1%[1%[3%[1', 'Sent pkg'], ['[1%[1%[2%[1', 'Received pkg']],
         'Sent/Received package'],                        # Sent/Received
    21: [id_audio,
         ["%ds", "%.1fkb"],
         [['[1%[1%[3%[0', 'Sent byte'], ['[1%[1%[2%[0', 'Received byte']],
         'Sent/Received byte'],                           # Sent/Received
    22: [id_audio,
         ["%ds", "%.1fms"],
         [['[1%[1%[5%[0', 'RTT']],
         'Echo RTT'],                                     # Echo RTT
    23: [id_audio,
         ["%ds", "%.1f"],
         [['[1%[1%[5%[1', 'Loss rate']],
         'Echo Loss rate'],                               # Echo Loss rate
    24: [id_audio,
         ["%ds", "%.1fms"],
         [['[1%[1%[0%[0', 'avg '], ['[1%[1%[0%[1', 'last']],
         'RTT'],                                          # RTT avg + last
    25: [id_audio,
         ["%ds", "%dp"],
         [['[1%[1%[4%[0', 'loss package rate'], [
             '[1%[1%[11%[1', 'package missing playback rate']],
         'Loss pkg rate'],                                # Loss pkg rate
    26: [id_audio,
         ["%ds", "%dp"],
         [['[1%[1%[1%[3', 'sent'], ['[1%[1%[1%[4', 'received'], [
             '[1%[1%[1%[5', 'recover'], ['[1%[1%[1%[7', 'real']],
         'FEC rate'],                                     # FEC rate
    27: [id_audio,
         ["%ds", "%dp"],
         [['[1%[1%[4%[1', 'loss package total'], [
             '[1%[1%[11%[0', 'package missing playback total']],
         'Loss pkg total'],                               # Loss pkg total
    28: [id_audio,
         ["%ds", "%dp"],
         [['[1%[1%[1%[0', 'sent'], ['[1%[1%[1%[1', 'received'], [
             '[1%[1%[1%[2', 'recover'], ['[1%[1%[1%[6', 'real']],
         'FEC total'],                                    # FEC total
    29: [id_audio,
         ['%ds', '%d'],
         [['[1%[1%[5%[2', 'Echo'], ['[1%[1%[7%[0', 'Quality'], [
             '[1%[1%[10%[0', 'Detected state'], ['[1%[1%[7%[2', 'Remote point']],
         'Point'],
    30: [id_audio,
         ['%d', '%dp'],
         [['[1%[1%[8%[0', 'Jitter']],  # ,['[1%[1%[8%[1','Network']
         'Delay'],
    32: [id_audio,
         ['%d', '%.2f'],
         [['[1%[1%[10%[3', 'max'], ['[1%[1%[10%[2', 'min'], [
             '[1%[1%[10%[1', 'Jitter estimate']],
         'Congest Detection'
         ],
    31: [id_audio,
         ['%d', '%.2fkbps'],
         [['[1%[1%[9%[1', 'Upload'], ['[1%[1%[9%[0', 'Download']],
         'Device current network rate'
         ],
    40: [id_video,
         ['%d', '%dms'],
         [['[1%[1%[0%[1', 'avg'], ['[1%[1%[0%[0', 'last value']],
         'RTT of Video RTCP'
         ],
    41: [id_video,
         ['%d', '%dpkg'],
         [['[1%[1%[1%[1', 'tx pkg'], ['[1%[1%[4%[1', 'rx pkg']],
         'TX & RX pkg'
         ],
    46: [id_video,
         ['%d', '%.2fb'],
         [['[1%[1%[1%[0', 'tx bytes'], ['[1%[1%[4%[0', 'rx bytes']],
         'TX & RX bytes'
         ],
    42: [id_video,
         ['%d', '%pkg'],
         [['[1%[1%[7%[0', 'rate'], ['[1%[1%[7%[2', 'loss ratio of remote']],
         'Video loss packet'
         ],
    43: [id_video,
         ['%d', '%d'],
         [['[1%[1%[8%[0', 'RTT'], ['[1%[1%[8%[1',
                                   'loss'], ['[1%[1%[8%[2', 'Point']],
         'Echo'
         ],
    44: [id_video,
         ['%d', '%.2f'],
         [['[1%[1%[2%[2', 'capture frame rate'], [
             '[1%[1%[2%[5', 'encode frame rate']],
         'Capture/Encode'
         ],
    45: [id_video,
         ['%d', '%.2fkbps'],
         [['[1%[1%[5%[2', 'Decode frame rate'], [
             '[1%[1%[5%[5', 'Render frame rate']],
         'Decode/Render'
         ],
}

ids_win1 = [20, 21, 22, 23, 26, 28]
ids_win2 = [25, 27, 24, 29, 30, 32]  # for other window
ids_win = [20, 21, 22, 23, 26, 28, 25, 27, 24, 29, 30, 32]
ids_win_video = [40, 41, 46, 42, 44, 45]

##ids_win1 = [3,5,4,6,7,10,1,2,8]

# 3 - Received rate (byte)
# 5 - Received Pkg rate
# 4 - Sent rate (byte)
# 6 - Sent Pkg rate
# 7 - Loss Pkg rate,
# 10 - Loss pkg total (Expected package)
# 1 - RTT Average, Last
# 2 - FEC: Rate
# 9 - FEC: Total
# 8 - Echo: RTT, Loss, Point (Ngoc - New)
