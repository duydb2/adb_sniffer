#!/usr/bin/env python

import os
import sys

local_path = os.path.abspath(os.path.split(__file__)[0])

import logcat.offline_chart2 as logcat

if __name__ == '__main__':
    sys.exit(logcat.main(sys.argv[1:]))
