#!/usr/bin/env python
import subprocess
import signal
import os
import sys

local_path = os.path.abspath(os.path.split(__file__)[0])

# os.environ['PATH'] += ";D:\\tools\\android-sdk\\platform-tools;" + os.environ['PATH']
os.environ['PATH'] = "%s;" % (os.path.join(local_path, os.pardir, 'bin')) + os.environ['PATH']

import time
import Queue
import threading
import logging
import datetime
import json
import re
import math
import random
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.ticker import FormatStrFormatter
from matplotlib.font_manager import FontProperties

threads = []
main_thread = threading.currentThread()
monitor = 1
data = []
save_data = False
device_serial_num = ''
m_proc = None

execfile(os.path.join(local_path, os.pardir, 'config.py'))
execfile(os.path.join(local_path, 'android_process'))
execfile(os.path.join(local_path, 'logcat_filter2.py'))
execfile(os.path.join(local_path, 'chart2.py'))

def strtime(tick, format = "%H:%M:%S"):
  return time.strftime(format, time.localtime(tick))

def update_data(data, cached_time_value, logid, parsed_json):
  # overflow: trim data seq
  if len(data) > 20000:
    print 'Data overflow'
  new_record = [strtime(cached_time_value, "%M:%S"), logid, parsed_json, cached_time_value]
  data.append(new_record)
  print new_record

ignore = 4
def print_log(log_str):
  print log_str
  global save_data
  global ignore
  rx_avg_value = 0
  tx_avg_value = 0
  rtt_value = 0
  cached_time_value = time.time()
  results = re.compile(
      r'(\{.*\}|\[.*\])', re.IGNORECASE | re.MULTILINE).search(log_str)

  if log_file:
    log_file.write("".join(log_str).replace("\r", ""))
  if results:
    parsed_json = json.loads(results.group(0))
    logid = int(parsed_json[0])
    dur = int(parsed_json[1][0])
    if dur < 3:
        save_data = True
    if save_data:
      update_data(data, cached_time_value, logid,  parsed_json)
    else:
      ignore -= 1
	
def logcat_capture():
  """capture log cat parse to data"""
  log_listener(print_log, call_log_file)
  logging.debug('ending')
  return
threads.append(threading.Thread(target=logcat_capture))
# add logcat thread

if monitor:
  def monitor():
    """monitor thread"""
    monitor_thread = threading.currentThread()
    while True:
		if m_proc:
			print m_proc.pid
		now = datetime.datetime.now()
		print '------ %s' % (now.strftime("%I_%M%p_%B_%d_%Y"))
		list_threads = threading.enumerate()
		list_threads.remove(monitor_thread)
		list_threads.remove(main_thread)
		for t in list_threads:
			if t.isAlive():
				logging.debug('living %s', t.getName())
		if not list_threads:
			break
		print '------------------------'
		time.sleep(1)
    logging.debug('ending')
    return
#threads.append(threading.Thread(target=monitor))
# add monitor thread

def test_thread():
  """create data for test realtime chart"""
  duration = 0.
  with open(call_log_file, "w") as my_log:
      my_log.write("")
  while True:
    duration += 1
    log_str = """ sakdjf;asjfl ;asjfd; [0,[%d,[[100,100],[100,100,100,3,1,2],[%.2f,%.2f],[%.2f,%.2f],[53, 100],[53, 100, 2],[100]]]]
""" % (duration,
       random.uniform(1, 10), # rx rate
       random.uniform(1, 10), # rx pkg rate
       random.uniform(1, 10), # tx rate
       random.uniform(1, 10)) # tx pkg rate
    with open(call_log_file, "a") as my_log:
      my_log.write(log_str)
    time.sleep(0.99)
  return
# threads.append(threading.Thread(target=test_thread))
# add logcat thread

##threads.append(threading.Thread(target=draw_charts))

def logcat_realtime():
  thread = threading.Thread(target=run_on_thread)
  thread.daemon = True
  thread.start()
  
  # draw chart on main thread
  draw_charts()

def run_on_thread():
  for t in threads:
    t.setDaemon(True)
    t.start()
  for t in threads:
    t.join()
  if log_file:
    log_file.close()


def main(argv):
  global call_log_file
  if len(argv) > 1:
    call_log_file = argv[1]
  print 'call log path: %s'%call_log_file
  logcat_realtime()


if __name__ == '__main__':
  main(sys.argv)
