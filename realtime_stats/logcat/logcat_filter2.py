import re
import subprocess
import time
import os

def log_listener(out, call_log, **kwargs):
	while True:
		print 'Waiting log file .... %s'%call_log
		if os.path.isfile(call_log):
			print 'File exist'
			break
		time.sleep(0.99)
  
	cmd = ['tail']
	cmd.extend(['-f', call_log])
	log_lines = ['LOG_BEGIN\n\n']
	# http://stackoverflow.com/questions/4789837/how-to-terminate-a-python-subprocess-launched-with-shell-true
	# non-block http://eyalarubas.com/python-subproc-nonblock.html
	print cmd
	proc = subprocess.Popen(cmd,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT,
                          bufsize=0)

	while True:
		line = proc.stdout.readline()
		if proc.poll() is not None:
			break
		if line:
			if len(log_lines) > 50000:
				log_lines = ['LOG_RESTART\n\n']
		out(line)
	return proc.poll()


