
def get_value(objson, key):
	try:
		result = objson
		keys = key.split('%')
		for k in keys:
			if k:
				rk = re.search(r'\{(.*)|\[(.*)',k)
				if rk:
					obj_key, list_key = rk.groups()
					if obj_key:
						result = result.get(obj_key, {})
					if list_key:
						result = result[int(list_key)]
				if not result:
					break
		
		if not result:
			return 0
		return float(result)
	except Exception:
		return 0
		
 
def getKey(item):
    return item[3]

def set_data(data, logid, lk):
    # filter with logid
    filter_data = [item for item in data if item[1] == logid]
    time_line = sorted(filter_data, key=getKey)
    x_line = [get_value(item[2], "[1%[0") for item in time_line]
    y_line = [get_value(item[2], lk) for item in time_line]
    xticklabels = x_line
    return  x_line, y_line, xticklabels
    
def update_plot(plot_map):
    now = time.time()
    lines = plot_map.get('lines')
    plot = plot_map.get('plot')
    charid = plot_map.get('charid')
    x_lim = plot_map.get('x_lim')
    y_lim = plot_map.get('y_lim')
    if lines:
        data_timeline = data
        for key,(line,logid, lk) in lines.items():
            x_line, y_line, xlabels = set_data(data_timeline, logid,  lk)
            if x_line and y_line:
                line.set_data(x_line,y_line)
                y_lim[0] = min(min(y_line), y_lim[0])
                y_lim[1] = max(max(y_line), y_lim[1])
                x_lim[0] = max(min(x_line), x_lim[0])
                x_lim[1] = max(max(x_line), x_lim[1])
##        print x_lim
##        print y_lim
        plot.set_xlim(x_lim[0] - 1 , x_lim[1] + 1)
        plot.set_ylim(y_lim[0] - 1 , y_lim[1] + 1)
##        plot.set_xlabel(label[0], fontdict=font)
##        plot.set_ylabel(label[1], fontdict=font)
    plot_map['x_lim'] = x_lim
    plot_map['y_lim'] = y_lim

def create_plot(fig, chartid, order):
    print 'create plot %s' % chartid 
    subplot = fig.add_subplot(3, 2, order)
    subplot.grid()
    lines = {}
    result = {}
    for i, lk in enumerate(plots[chartid][2]):
        line, = subplot.plot([], label='%s' % lk[1])
        lines.update({i:[line, plots[chartid][0], lk[0]]})
    result['chartid'] = chartid
    result['plot'] = subplot
    result['lines'] = lines
    result['x_lim'] = [0,1]
    result['y_lim'] = [0,1]
    subplot.set_title(plots[chartid][3])
    legend = subplot.legend( loc='upper left', ncol=1, prop = fontP, fancybox=True, shadow=False, fontsize = 'x-small')
    plt.gca().xaxis.set_major_formatter(FormatStrFormatter(plots[chartid][1][0]))
    plt.gca().yaxis.set_major_formatter(FormatStrFormatter(plots[chartid][1][1]))
    return result

def update(i, plots):
    if len(data) > 3:
      for plot_map in plots:
          update_plot(plot_map)
        
    # add text for plot
    return 


def draw_charts():
  """draw chart threads"""
  plt.close('all')
  fig = plt.figure(1)
  plots = [create_plot(fig, chartid, order + 1) for order, chartid in enumerate(ids_win1)]
  fig.subplots_adjust(left = 0.04 , bottom = 0.05, right = 0.98 , top = 0.95,
                wspace = 0.15 , hspace = 0.15)
  fig.canvas.set_window_title(device_serial_num)
  ani1 = animation.FuncAnimation(fig, func=update, fargs=([plots]),
                                          interval=999, blit=False, repeat=False)

  fig = plt.figure(2)
  plots2 = [create_plot(fig, chartid, order + 1) for order, chartid in enumerate(ids_win2)]
  fig.subplots_adjust(left = 0.04 , bottom = 0.05, right = 0.98 , top = 0.95,
                wspace = 0.15 , hspace = 0.15)
  fig.canvas.set_window_title(device_serial_num)
  ani2 = animation.FuncAnimation(fig, func=update, fargs=([plots2]),
                                          interval=999, blit=False, repeat=False)

  plt.show()
  logging.debug('ending')
