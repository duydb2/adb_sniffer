#!/usr/bin/env python
import subprocess as sp
import signal
import os
import sys

import time
import Queue
import logging
import datetime
import json
import re
import math
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.ticker import FormatStrFormatter
from matplotlib.font_manager import FontProperties
import gzip
import matplotlib.lines as lines
import numpy
from cStringIO import StringIO

script_path = os.path.abspath(os.path.split(__file__)[0])

os.environ['PATH'] += ";D:\\tools\\android-sdk\\platform-tools;" + \
    os.environ['PATH']
os.environ['PATH'] += ";%s;" % (os.path.join(script_path,
                                             os.pardir, 'bin')) + os.environ['PATH']

# print os.environ['PATH']


execfile(os.path.join(script_path, os.pardir, 'config.py'))


def get_value(objson, key):
    try:
        result = objson
        keys = key.split('%')
        for k in keys:
            if k:
                rk = re.search(r'\{(.*)|\[(.*)', k)
                if rk:
                    obj_key, list_key = rk.groups()
                    if obj_key:
                        result = result.get(obj_key, {})
                    if list_key:
                        result = result[int(list_key)]
                if not result:
                    break

        if not result:
            return 0
        return float(result)
    except Exception:
        return 0

def create_data():
    i = 0
    yield i++

def make_plot_data(data, plot_info):
    plot_data = [][]

def create_plot(fig, data, numplots, chartid, order):
    col = 2
    row = numplots // col + 0 if numplots % col == 0 else 1
    subplot = fig.add_subplot(row, col, order)
    subplot.grid()
    result = {}
    x_line = [get_value(item, "[1%[0") for item in data]
    for (i, lk) in enumerate(plots[chartid][2]):
        # get data of line with key lk[0]
        _lk = lk[0]
        if _lk == '[1%[1%[7%[2':
            y_line = [get_value(item, _lk) * 5 for item in data]
        else:
            y_line = [get_value(item, _lk) for item in data]
        line, = subplot.plot(x_line, y_line, label='%s' % lk[1], picker=5)
    subplot.relim()
    subplot.set_xlim(0, max(x_line))
    subplot.autoscale_view(True, True, True)
    subplot.set_title(plots[chartid][3])
    legend = subplot.legend(loc='upper left', ncol=5, prop=fontP,
                            fancybox=True, shadow=False, fontsize='x-small')
    plt.gca().xaxis.set_major_formatter(
        FormatStrFormatter(plots[chartid][1][0]))
    plt.gca().yaxis.set_major_formatter(
        FormatStrFormatter(plots[chartid][1][1]))
    print 'create chart %s' % (plots[chartid][3])
    return result


def draw_charts(data_source):
    fig = plt.figure(figsize=(22, 60), dpi=75)
    draw(fig, data_source.read)
    fig.suptitle(data_source.name())

current = {
    'axis': None,
    'x': 0,
    'y': 0,
}


def update_current(paxis):
    xd = current['x']
    yd = current['y']
    axis = current['axis']
    if axis and xd and yd:
        # print len(axis.texts), len(axis.lines)
        caption = ""
        for line in axis.lines:
            x = 0
            try:
                x = numpy.where(line.get_xdata() == math.floor(xd))[0]
                if len(x) == 0:
                    x = 0
            except ValueError:
                x = max(line.get_xdata())
            caption = caption + "%2.2f, " % (line.get_ydata()[x])
        for text in axis.texts:
            text.remove()
        axis.text(math.floor(xd), math.floor(yd),
                  "[%2d\n(%s)]" % (math.floor(xd), caption))
        # axis.plot(math.floor(xd), math.floor(yd), color='rs')
        axis.figure.canvas.draw()


def draw(fig, make_data):
    fig.clf()
    data = make_data()
    print data
    plots = [create_plot(fig, data, len(ids_win_video), chartid, order + 1)
             for order, chartid in enumerate(ids_win_video)]
    fig.subplots_adjust(left=0.04, bottom=0.05, right=0.92, top=0.95,
                        wspace=0.15, hspace=0.15)

    def onpress(event):
        # print event
        current['axis'] = event.inaxes
        current['x'] = event.xdata
        current['y'] = event.ydata
        update_current(current)

    def onclick(event):
        if event.key == 'right':
            current['x'] = current['x'] + 1
            update_current(current)

        if event.key == 'left':
            current['x'] = max(current['x'] - 1, 0)
            update_current(current)

        if event.key == 'up':
            current['y'] = current['y'] + 1
            update_current(current)

        if event.key == 'down':
            current['y'] = max(current['y'] - 1, 0)
            update_current(current)

        # print event.key
        return False

    fig.canvas.mpl_connect('key_press_event', onclick)
    fig.canvas.mpl_connect('button_press_event', onpress)
    return fig


def offline_charts(data_source):
    """draw chart threads"""
    draw_charts(data_source)


class SSH(object):
    title = ''

    def __init__(self, path):
        self.data_path = path.replace('ssh://', '')

    def read_ssh_data(self):
        p = sp.Popen('''
#!/bin/bash
ssh call_log "$( cat << EOT
SSH_AUTH_SOCK=/tmp/ssh-pSmGQ22074/agent.22074; export SSH_AUTH_SOCK;
SSH_AGENT_PID=22075; export SSH_AGENT_PID;
ssh call_log2 'cat %s'
EOT)"
        ''' % (self.data_path), shell=True, stdout=sp.PIPE)
        return p.communicate()[0]

    def read(self):
        zipdata = StringIO()
        zipdata.write(self.read_ssh_data())
        zipdata.seek(0)
        data = []
        with gzip.GzipFile(
                fileobj=zipdata,
                mode='rb') as zipfile:
            for line in zipfile:
                data = self.process_line(data, line)
        print 'read data from path: %s' % (self.data_path)
        return data

    def process_line(self, data, line):
        if line:
            results = re.compile(
                r'(\{.*\}|\[.*\])', re.IGNORECASE | re.MULTILINE).search(line)
            if results:
                results = results.group(0)
                results = results.replace("Inf", "0")
                results = results.replace("inf", "0")
                str_json = json.loads(results)
                if str_json[0] == 0:
                    # print str_json
                    data.append(str_json)
                else:
                    self.title += '_%s_' % str(str_json[1])
                    # print str_json
        return data

    def fileread(self, path):
        data = []
        if path:
            basename, ext = os.path.splitext(os.path.basename(path))
            if '.gz' == ext:
                with gzip.open(path, 'rb') as zipfile:
                    for line in zipfile:
                        data = self.process_line(data, line)
            else:
                with open(path, 'rb') as file:
                    for line in file:
                        data = self.process_line(data, line)
        return data

    def name(self):
        return self.title


class Local(object):
    title = ''

    def __init__(self, path):
        self.data_path = path

    def read(self,):
        return self.fileread(self.data_path)

    def process_line(self, data, line):
        if line:
            results = re.compile(
                r'(\{.*\}|\[.*\])', re.IGNORECASE | re.MULTILINE).search(line)
            if results:
                results = results.group(0)
                results = results.replace("Inf", "0")
                results = results.replace("inf", "0")
                str_json = json.loads(results)
                if str_json[0] in [0, 3]:
                    # print str_json
                    data.append(str_json)
                else:
                    self.title += '_%s_' % str(str_json[1])
                    # print str_json
        return data

    def fileread(self, path):
        data = []
        if path:
            basename, ext = os.path.splitext(os.path.basename(path))
            if '.gz' == ext:
                with gzip.open(path, 'rb') as zipfile:
                    for line in zipfile:
                        data = self.process_line(data, line)
            else:
                with open(path, 'rb') as file:
                    for line in file:
                        data = self.process_line(data, line)
        return data

    def name(self):
        return self.title


def main(argv):
    ssh_path = []
    local_path = []

    if argv[0] == 'local':
        exec('local_path=%s' % argv[1])
        for data_path in local_path:
            print data_path
            offline_charts(Local(data_path))
    else:
        exec('ssh_path=%s' % argv[0])
        for data_path in ssh_path:
            offline_charts(SSH(data_path))

    plt.show()


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
