#!/usr/bin/env python
import subprocess
import signal
import os
import sys

local_path = os.path.abspath(os.path.split(__file__)[0])

os.environ['PATH'] += ";D:\\tools\\android-sdk\\platform-tools;" + \
    os.environ['PATH']
os.environ['PATH'] += ";%s;" % (os.path.join(local_path,
                                             os.pardir, 'bin')) + os.environ['PATH']

# print os.environ['PATH']

import time
import Queue
import logging
import datetime
import json
import re
import math
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.ticker import FormatStrFormatter
from matplotlib.font_manager import FontProperties
import gzip

execfile(os.path.join(local_path, os.pardir, 'config.py'))
execfile(os.path.join(local_path, 'chart_offline.py'))


data = []
device_serial_num = ''


def strtime(tick, format="%H:%M:%S"):
    return time.strftime(format, time.localtime(tick))


def update_data(data, cached_time_value, logid, parsed_json):
    # overflow: trim data seq
    if len(data) > 20000:
        print 'Data overflow'
    new_record = [strtime(cached_time_value, "%M:%S"),
                  logid, parsed_json, cached_time_value]
    data.append(new_record)
    # print new_record


def print_log(log_str):
    # print log_str
    rx_avg_value = 0
    tx_avg_value = 0
    rtt_value = 0
    cached_time_value = time.time()
    results = re.compile(
        r'(\{.*\}|\[.*\])', re.IGNORECASE | re.MULTILINE).search(log_str)

    if results:
        print results.group(0)
        parsed_json = json.loads(results.group(0))
        logid = int(parsed_json[0])
        dur = int(parsed_json[1][0])
        update_data(data, cached_time_value, logid, parsed_json)


def read(data_path):
    basename, ext = os.path.splitext(os.path.basename(data_path))
    if '.gz' == ext:
        with gzip.open(data_path, 'rb') as zipfile:
            for line in zipfile:
                print_log(line)
    else:
        with open(data_path, 'rb') as file:
            for line in file:
                print_log(line)


def offline_charts(data_path):
    """draw chart threads"""
    filename_path = os.path.abspath(data_path)
    print filename_path
    read(data_path)
    draw_charts(data_path)


def main(argv):
    data_path = data_file_path
    if len(argv) > 0:
        data_path = argv[0]
    offline_charts(data_path)

if __name__ == '__main__':
    sys.exit(main(argv[1:]))
