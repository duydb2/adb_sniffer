#!/usr/bin/env python
import subprocess
import signal
import os
import sys

local_path = os.path.abspath(os.path.split(__file__)[0])
os.environ['PATH'] = "%s;" % (os.path.join(local_path, os.pardir, 'bin')) + os.environ['PATH']

from Tkinter import *

import subprocess
import threading

## Utils library  
def get_devices_list():
      cmd = ['adb']
      cmd.extend(['devices'])

      proc = subprocess.Popen(cmd,
                     stdout=subprocess.PIPE,
                     stderr=subprocess.STDOUT,
                     stdin=subprocess.PIPE)
      processes = proc.communicate()[0].split("\r\n")
      results = []
      for item in processes:
        if 'device' in item:
           if item:
              results.append(item.split()[0])
      return results

def start_chart(devie_number):
      # cmd = ['cmd', '/k', sys.executable]
      cmd = ['python']
      cmd.extend([os.path.join(local_path,'realtime_chart.py'), devie_number])
      print cmd
      proc = subprocess.Popen(cmd, close_fds = True).pid # creationflags=0x00000008, 
      # proc = subprocess.Popen(cmd,
                              # stdout=subprocess.PIPE,
                              # stderr=subprocess.STDOUT,
                              # stdin=subprocess.PIPE)
      # proc_out = proc.communicate()[0]
      # print proc_out

class SnifferApp:  
  def __init__(self, master):
    frame = Frame(master)
    frame.pack(fill=X)
    self.offline_chart = Button(frame, 
                         text="offline_chart", fg="red",
                         command=self.offline_chart)
##    self.offline_chart.pack(fill=X)
    
    
    self.reset_realchart = Button(frame,
                         text="Get devices list",
                         command=self.refesh_device_list)
    self.reset_realchart.pack(fill=X)

    self.label = Label( frame, text="List devices:", relief=RAISED )
    self.label.pack(fill=BOTH)

    self.device_list = Listbox(frame,selectmode=SINGLE)
    self.device_list.pack(fill=BOTH, expand=1)    

    self.realtime_chart = Button(frame,
                         text="realtime_chart",
                         command=self.realtime_chart)
    self.realtime_chart.pack(fill=X)  

    self.reset_realchart = Button(frame,
                         text="reset_realchart",
                         command=self.reset_realchart)
    self.reset_realchart.pack(fill=X)

  def refesh_device_list(self):
    list_size = self.device_list.size()
    self.device_list.delete(0,list_size)
    
    list_device = get_devices_list()
    if list_device:        
        for item in list_device:
            self.device_list.insert(END, item)
        self.device_list.activate(0)

 
  def realtime_chart(self):
    selected_id = self.device_list.curselection()
    if selected_id:
        selected = self.device_list.get(selected_id)
        print selected
        thread = threading.Thread(target=start_chart, args=(selected,))
        thread.daemon = True
        thread.start()
    
  def offline_chart(self):
    print "offline_chart"

  def reset_realchart(self):
    print "reset_realchart"

  def quit(self):
    self.master.quit()
 
def main():
    root = Tk()
    app = SnifferApp(root)
    root.mainloop()

if __name__ == "__main__":
    main()
