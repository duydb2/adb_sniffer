#!/usr/bin/env python
import subprocess
import signal
import os
import sys

local_path = os.path.abspath(os.path.split(__file__)[0])

# os.environ['PATH'] += ";D:\\tools\\android-sdk\\platform-tools;" + os.environ['PATH']
os.environ['PATH'] = "%s;" % (os.path.join(
    local_path, os.pardir, 'bin')) + os.environ['PATH']

import time
import Queue
import threading
import logging
import datetime
import json
import re
import math
import random
import matplotlib
# matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.ticker import FormatStrFormatter
from matplotlib.font_manager import FontProperties

threads = []
main_thread = threading.currentThread()
monitor = 1
data = []
save_data = False
device_serial_num = ''
m_proc = None

execfile(os.path.join(local_path, 'android_process'))
execfile(os.path.join(local_path, 'logcat_filter'))
execfile(os.path.join(local_path, os.pardir, 'config.py'))
execfile(os.path.join(local_path, 'chart3.py'))


def strtime(tick, format="%H:%M:%S"):
    return time.strftime(format, time.localtime(tick))


def update_data(data, cached_time_value, logid, parsed_json):
    # overflow: trim data seq
    if len(data) > 20000:
        print 'Data overflow'
    new_record = [strtime(cached_time_value, "%M:%S"),
                  logid, parsed_json, cached_time_value]
    data.append(new_record)
    print "dudyb2 ---> ", new_record

ignore = 4


def print_log(log_str):
    global save_data
    global ignore
    rx_avg_value = 0
    tx_avg_value = 0
    rtt_value = 0
    cached_time_value = time.time()
# results = re.compile(
# r'({.*"id".*:.*,.*"timestamp".*:.*})', re.IGNORECASE |
# re.MULTILINE).search(log_str)
    results = re.compile(
        r'(\{.*\}|\[.*\])', re.IGNORECASE | re.MULTILINE).search(log_str)

    if log_file:
        log_file.write("".join(log_str).replace("\r", ""))


    if results:
        # print results.group(0)
        parsed_json = json.loads(results.group(0))
        logid = int(parsed_json[0])
        if logid in [0, 3]:
            dur = int(parsed_json[1][0])
            if dur < 3:
                save_data = True
            if save_data:
                update_data(data, cached_time_value, logid, parsed_json)
            else:
                ignore -= 1


def selector(l, conds):
    return True
    if conds:
        for item in conds:
            if '%s' % item in l:
                return True
    return False


def get_proc(proc):
    global m_proc
    m_proc = proc


def logcat_capture():
    """capture log cat parse to data"""

    # if not log_file:
    #  log_file = open('%s_%s.txt' % (__file__.split('.')[0], time.strftime("%I_%M%p_%B_%d_%Y")), 'w')
    process_name = 'com.zing.zalo'
    clear_adb(device_serial_num)
    cond_list = []
    cond_list.extend(get_pid_pname(
        device_serial_num, 'com.csipsimple:sipStack'))
    # cond_list.append('DEBUG')
    # get log cat by process num
    logcat_selector_filter(selector,
                           [cond_list],
                           r'.*histogram.*(\[.*\]|\{.*\})',
                           print_log,
                           device_serial=device_serial_num,
                           get_proc=get_proc)
    logging.debug('ending')
    return
threads.append(threading.Thread(target=logcat_capture))
# add logcat thread

if monitor:
    def monitor():
        """monitor thread"""
        monitor_thread = threading.currentThread()
        while True:
            if m_proc:
                print m_proc.pid
            now = datetime.datetime.now()
            print '------ %s' % (now.strftime("%I_%M%p_%B_%d_%Y"))
            list_threads = threading.enumerate()
            list_threads.remove(monitor_thread)
            list_threads.remove(main_thread)
            for t in list_threads:
                if t.isAlive():
                    logging.debug('living %s', t.getName())
            if not list_threads:
                break
            print '------------------------'
            time.sleep(1)
        logging.debug('ending')
        return
    threads.append(threading.Thread(target=monitor))
# add monitor thread


def test_thread():
    """create data for test realtime chart"""
    duration = 0
    while True:
        duration += 1
        log_str = """ sakdjf;asjfl ;asjfd;
[5,[%d,[[100,100],[100,100,100,3,1,2],[%.2f,%.2f],[%.2f,%.2f],[53, 100],[53, 100, 2],[100]]]]
""" % (duration,
            random.uniform(1, 10),  # rx rate
            random.uniform(1, 10),  # rx pkg rate
            random.uniform(1, 10),  # tx rate
            random.uniform(1, 10))  # tx pkg rate
        print_log(log_str)
        time.sleep(0.99)
    return
# threads.append(threading.Thread(target=test_thread))
# add logcat thread

# threads.append(threading.Thread(target=draw_charts))


def logcat_realtime():
    thread = threading.Thread(target=run_on_thread)
    thread.daemon = True
    thread.start()

    # draw chart on main thread
    draw_charts()


def run_on_thread():
    for t in threads:
        t.setDaemon(True)
        t.start()
    for t in threads:
        t.join()
    if log_file:
        log_file.close()


def main(argv):
    global device_serial_num
    print argv
    if len(argv) > 1:
        device_serial_num = argv[1]
    print device_serial_num
    logcat_realtime()


if __name__ == '__main__':
    main(sys.argv)
