#!/usr/bin/env python
import signal
import os
import sys
import time
import datetime
local_path = os.path.abspath(os.path.split(__file__)[0])
execfile(os.path.join(local_path, 'android_process'))
execfile(os.path.join(local_path, 'logcat_filter'))

log_lines = ['LOG_BEGIN\n\n']
log_msg = 1
fileter_str = '--end msg--'

def exit_gracefully(signum, frame=None):
  try:
    answer = raw_input("\nReally quit? (y/n)? > ")
    if answer.lower().startswith('y'):
      global log_lines
      now = datetime.datetime.now()
      log_file = open('%s_%s.txt' %
                      (__file__.split('.')[0], now.strftime("%I_%M%p_%B_%d_%Y")), 'w')
      log_file.write("".join(log_lines).replace("\r", ""))
      log_file.close()
      sys.exit(1)

  except KeyboardInterrupt:
    print("Ok ok, quitting")
    sys.exit(1)

  # restore the original signal handler as otherwise evil things will happen
  # in raw_input when CTRL+C is pressed, and our signal handler is not
  # re-entrant
  signal.signal(signal.SIGINT, exit_gracefully)
  signal.signal(signal.SIGTERM, exit_gracefully)
  signal.signal(signal.SIGBREAK, exit_gracefully)


def process(log_str):
  print_num = 45
  global log_lines

  if len(log_lines) > 100000:
    log_lines = ['LOG_RESTART\n\n']  # reset cache log overflow

  log_lines.append(log_str)  # cache log
  if len(log_lines) > print_num:
    if fileter_str in log_str:
      print_line = log_lines[-print_num:]
      # print print_line
      if log_msg:
        # find candidate
        index_candidate = print_num
        before_line_num = 10
        begin_from_str = ''
        while index_candidate > 0:
          index_candidate = index_candidate - 1
          if 'SIP/2.0 ' in print_line[index_candidate]:
            break
          if ' SIP/2.0' in print_line[index_candidate]:
            break
        print_num = index_candidate - before_line_num - 1
      # filter message package
      msg = ''.join(print_line[print_num:])
      if '' in msg:
        print msg
        print '------------------'


def selector(l, conds):
  return True
  if conds:
    for item in conds:
      if '%s' % item in l:
        return True
  return False

if __name__ == '__main__':
  # store the original SIGINT handler
  # original_sigint = signal.getsignal(signal.SIGINT)
  signal.signal(signal.SIGINT, exit_gracefully)
  signal.signal(signal.SIGTERM, exit_gracefully)
  signal.signal(signal.SIGBREAK, exit_gracefully)
  device_serial_num = '0123456789ABCDEF'  # '0123456789ABCDEF' # 'HT334LP00027'


  cond_list = []
  cond_list.extend(get_pid_pname(device_serial_num, 'com.csipsimple:sipStack'))
  cond_list.append('DEBUG')
  # get log cat by process num
  logcat_selector(selector, [cond_list], cond=lambda x: True, out=process, device_serial = device_serial_num)
