#!/usr/bin/env python
import sys
import os

local_path = os.path.dirname(os.path.abspath(__file__))

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wxagg import NavigationToolbar2WxAgg as NavigationToolbar
from matplotlib.figure import Figure 
import numpy 
import wx 
import wx.lib.scrolledpanel as SP 
import matplotlib.animation as manim
import offline_chart2 as chart

# http://wxpython-users.1045709.n5.nabble.com/Very-slow-scrolling-with-multiple-plots-in-window-td2357219.html
# http://stackoverflow.com/questions/4740988/add-new-navigate-modes-in-matplotlib

class MyNavToolbar(NavigationToolbar):
    """wx/mpl NavToolbar hack with an additional tools user interaction.
    This class is necessary because simply adding a new togglable tool to the
    toolbar won't (1) radio-toggle between the new tool and the pan/zoom tools.
    (2) disable the pan/zoom tool modes in the associated subplot(s).
    """
    ID_LASSO_TOOL = wx.NewId()
    def __init__(self, canvas):
        super(NavigationToolbar, self).__init__(canvas)

        self.pan_tool  = self.FindById(self._NTB2_PAN)
        self.zoom_tool = self.FindById(self._NTB2_ZOOM)

        self.lasso_tool = self.InsertSimpleTool(5, self.ID_LASSO_TOOL, 
                            wx.ArtProvider.GetBitmap(wx.ART_ADD_BOOKMARK),
                            isToggle=True)
        self.Bind(wx.EVT_TOOL, self.on_toggle_lasso_tool, self.lasso_tool)
        self.Bind(wx.EVT_TOOL, self.on_toggle_pan_zoom, self.zoom_tool)
        self.Bind(wx.EVT_TOOL, self.on_toggle_pan_zoom, self.pan_tool)

    def get_mode(self):
        """Use this rather than navtoolbar.mode
        """
        if self.lasso_tool.IsToggled():
            return 'lasso'
        else:
            return self.mode

    def untoggle_mpl_tools(self):
        """Hack city: Since I can't figure out how to change the way the 
        associated subplot(s) handles mouse events: I generate events to turn
        off whichever tool mode is enabled (if any). 
        This function needs to be called whenever any user-defined tool 
        (eg: lasso) is clicked.
        """
        if self.pan_tool.IsToggled():
            wx.PostEvent(
                self.GetEventHandler(), 
                wx.CommandEvent(wx.EVT_TOOL.typeId, self._NTB2_PAN)
            )
            self.ToggleTool(self._NTB2_PAN, False)
        elif self.zoom_tool.IsToggled():
            wx.PostEvent(
                self.GetEventHandler(),
                wx.CommandEvent(wx.EVT_TOOL.typeId, self._NTB2_ZOOM)
            )
            self.ToggleTool(self._NTB2_ZOOM, False)

    def on_toggle_lasso_tool(self, evt):
        """Lasso tool handler.
        """
        if evt.Checked():
            self.untoggle_mpl_tools()

    def on_toggle_pan_zoom(self, evt):
        """Called when pan or zoom is toggled. 
        We need to manually untoggle user-defined tools.
        """
        if evt.Checked():
            self.ToggleTool(self.ID_LASSO_TOOL, False)
        # Make sure the regular pan/zoom handlers get the event
        evt.Skip()

class ScatterPanel(FigureCanvas):
    """Contains the guts for drawing scatter plots.
    """
    def __init__(self, parent, **kwargs):
        self.figure = Figure()
        FigureCanvasWxAgg.__init__(self, parent, -1, self.figure, **kwargs)
        self.canvas = self.figure.canvas
        self.SetMinSize((100,100))
        self.figure.set_facecolor((1,1,1))
        self.figure.set_edgecolor((1,1,1))
        self.canvas.SetBackgroundColour('white')

        self.subplot = self.figure.add_subplot(111)
        self.navtoolbar = None
        self.lasso = None
        self.redraw()

        self.canvas.mpl_connect('button_press_event', self.on_press)
        self.canvas.mpl_connect('button_release_event', self.on_release)

    def lasso_callback(self, verts):
        pass

    def on_press(self, evt):
        """canvas mousedown handler
        """
        if evt.button == 1:
            if self.canvas.widgetlock.locked(): 
                return
            if evt.inaxes is None: 
                return
            if self.navtoolbar and self.navtoolbar.get_mode() == 'lasso':
                self.lasso = Lasso(evt.inaxes, (evt.xdata, evt.ydata), self.lasso_callback)
                self.canvas.widgetlock(self.lasso)

    def on_release(self, evt):
        """canvas mouseup handler
        """
        # Note: lasso_callback is not called on click without drag so we release
        #   the lock here to handle this case as well.
        if evt.button == 1:
            if self.lasso:
                self.canvas.draw_idle()
                self.canvas.widgetlock.release(self.lasso)
                self.lasso = None
        else:
            self.show_popup_menu((evt.x, self.canvas.GetSize()[1]-evt.y), None)

    def redraw(self):
        self.subplot.clear()
        self.subplot.scatter([1,2,3],[3,1,2])

    def get_toolbar(self):
        if not self.navtoolbar:
            self.navtoolbar = MyNavToolbar(self.canvas)
            self.navtoolbar.Realize()
        return self.navtoolbar


class MainFrame(wx.Frame):
    def __init__(self):
        no_caption =  wx.DEFAULT_FRAME_STYLE | wx.MAXIMIZE_BOX  #wx.MINIMIZE_BOX | wx.MAXIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CLOSE_BOX | wx.CLIP_CHILDREN
        wx.Frame.__init__(self, None, -1, 'CanvasFrame', style=no_caption)
        self.SetBackgroundColour(wx.NamedColor('WHITE'))
        self.splitter = wx.SplitterWindow(self)

        self.plotpanel = PlotPanel(self)
        
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer) 
        self.sizer.Add(self.plotpanel, 1, wx.EXPAND)
        self.sizer.Add(ToolbarPanel(self, self.plotpanel.canvas), 0, wx.TOP)
        self.plotpanel.update_figure('')
        
        self.update = self.animate().next
        self.timer = wx.Timer(self)
        self.timer.Start(1)

    def animate(self):
        print 'animate'
        

    def load_figure(self, path):
        self.plotpanel.update_figure(path)
   
class ToolbarPanel(wx.Panel):
    def __init__(self, parent, canvas):
        wx.Panel.__init__(self,parent, -1)
        self.toolbar = NavigationToolbar(canvas)
        self.toolbar.Realize()
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizerAndFit(sizer)

        sizer.Add(self.toolbar)
        self.toolbar.update()
        

class PlotPanel(SP.ScrolledPanel):
    def __init__(self, parent):
        SP.ScrolledPanel.__init__(self, parent, -1 , size=(200, 200), style = wx.TAB_TRAVERSAL|wx.SUNKEN_BORDER)
        self.SetBackgroundColour(wx.NamedColor('WHITE'))
        vbox = wx.BoxSizer(wx.VERTICAL)
        self.SetSizerAndFit(vbox)
        self.figure  = Figure(figsize=(22,60),dpi=75)
        self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self, -1 , self.figure)
        vbox.Add(self.canvas, 0, wx.EXPAND) # 0, wx.ALIGN_LEFT|wx.ALL, 5
        
        self.SetAutoLayout(1)
        self.SetupScrolling()

    def update_figure(self, path):
        self.figure = chart.draw(self.figure, path)
        self.canvas.draw()
        self.Refresh()
        



class App2(wx.App):
    def OnInit(self):
        self.frame = MainFrame()
        self.frame.Show(True)
        self.frame.Maximize(True)
        return True

def main(argv):
    app = App2(redirect=False)
    # app.frame.load_figure('')
    app.MainLoop()

class App(wx.App): 
    def DoDraw(self, path): 
        self.frame = wx.Frame(parent = None, 
        title = 'Scrollbar Example', size=(800, 600)) 
        vbox = wx.BoxSizer(wx.VERTICAL) 
        self.panel = SP.ScrolledPanel(self.frame, -1) 
        fig = Figure(figsize=(22,60),dpi=75)
        self.fig = chart.draw(fig,'') 
        self.canvas = FigureCanvas(self.panel, -1, self.fig) 
        vbox.Add(self.canvas, 1, wx.EXPAND) 

        self.panel.SetSizer(vbox) 
        self.panel.SetAutoLayout(1) 
        self.panel.SetupScrolling() 
        self.frame.Show()
        return True 
    
def main2(argv):
    path = ''
    if len(argv) > 0:
        path = argv[0]
    app = App(redirect = False)
    app.DoDraw(path)
    app.MainLoop() 

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

