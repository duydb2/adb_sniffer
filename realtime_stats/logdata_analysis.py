#!/usr/bin/env python
from __future__ import print_function

import os
import sys

local_path = os.path.dirname(os.path.abspath(__file__))

import subprocess
import hashlib
import matplotlib.pyplot as plt
import gzip
import re
import json

tmp_path = os.path.join(local_path,"tmp")
if not os.path.exists(tmp_path):
    os.makedirs(tmp_path)

def read(data_path):
    data = {}
    matching = {
        '[1%[0': 0,
        '[1%[1%[3%[1':1, # sent
        '[1%[1%[2%[1':2, # received
        '[1%[1%[10%[0': 3, # is congest
    }
    
    def get_value(objson, key):
        try:
            result = objson
            keys = key.split('%')
            for k in keys:
                if k:
                    rk = re.search(r'\{(.*)|\[(.*)',k)
                    if rk:
                        obj_key, list_key = rk.groups()
                        if obj_key:
                            result = result.get(obj_key, {})
                        if list_key:
                            result = result[int(list_key)]
                    if not result:
                        break
    
            if not result:
                return 0
            return float(result)
        except Exception:
            return 0
    
    def process_line(line):
        if line:
            results = re.compile(r'(\{.*\}|\[.*\])', re.IGNORECASE | re.MULTILINE).search(line)
            if results:
                results = results.group(0)
                results = re.sub("[Ii]nf","0",results)
                # print(results)
                jobject = json.loads(results)
                for k,v in matching.items():
                    data.setdefault(v, []).append(get_value(jobject,k))
        return data
    if data_path:
        basename, ext = os.path.splitext(os.path.basename(data_path))
        if '.gz' == ext:
            with gzip.open(data_path, 'rb') as zipfile:
                for line in zipfile:
                    data = process_line(line)
        else:
            with open(data_path,'rb') as file:
                for line in file:
                    data = process_line(line)
    return data



def hashfile(path, blocksize = 65536):
    afile = open(path, 'rb')
    hasher = hashlib.md5()
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    afile.close()
    return hasher.hexdigest()

def get_data_files(dirs, exten = '.csv'):
    rs = {}
    for topdir in dirs:
        for root, subdirs, files in os.walk(topdir):
            for name in files:
                if name.lower().endswith(exten):
                    path = os.path.join(root,name)
                    file_hash = hashfile(path)
                    rs.setdefault(file_hash,[]).append(path)
    return rs

def get_user_id(path):
    basename, exten = os.path.splitext(os.path.basename(path))
    return basename.split('_')[0]
    

def fileter_call_ids(files):
    map_id = {}
    rs = {}
    call = {}
    found = 0
    user_ids = {}
    
    def problem():
        found = 0
        for k,v in map_id.items():
            is_found = 0
            uid1 = ''
            uid2 = ''
            if len(v) > 1:
                if filtered(k, v[0]):
                    is_found += 1
                    uid1 = get_user_id(v[0])
                    user_ids[uid1] =  user_ids.setdefault(uid1 , 0) + 1
                    
                if filtered(k, v[1]):
                    is_found += 1        
                    uid2 = get_user_id(v[0])        
                    user_ids[uid2] =  user_ids.setdefault(uid2 , 0) + 1
                    
            elif len(v) > 0:
                if filtered(k, v[0]):
                    is_found += 1
                    uid1 = get_user_id(v[0])
                    user_ids[uid1] =  user_ids.setdefault(uid1 , 0) + 1
                                    
            if is_found > 0:
                print("%s %4d %10s %10s\n" % (k,is_found, uid1,uid2))
                found += 1
                call.update({id:v})
                
        return found
        
    def problem2():
        found = 0
        for k,v in map_id.items():
            is_found = 0
            if len(v) > 1:
                if filtered2(k, v[0]):
                    is_found += 1
                    
                if filtered2(k, v[1]):
                    is_found += 1        
                    
            elif len(v) > 0:
                if filtered2(k, v[0]):
                    is_found += 1
                                    
            if is_found > 0:
                # print("%s %4d \n" % (k,is_found))
                # found += 1
                call.update({k:v})
                
        return found
    
    for path in files:
        basename, exten = os.path.splitext(os.path.basename(path))
        id = basename.split('_')[2]
        item = map_id.setdefault(id,[])
        item.append(path)
        if len(item) > 2:
            print("item > 3 ", item)
            
    found = problem2()
    
    rs = call
    print(len(files), len(map_id),len(rs))
    print(user_ids)
   
    return rs

def filtered2(id, path):
    rs = False
    curr = read(path)
    dur = len(curr[0])
    is_congest = [item > 1 for item in curr[3]]
    
    rs = is_congest.count(True) > 0
    return rs

def filtered(id, path):
    rs = False
    curr = read(path)
    dur = len(curr[0])
    max_f_15s = 0
    f_15s_count = 0
    
    if dur > 4:
        _15s = curr[1][3:15]
        max_f_15s = max(_15s)
        m_pos_f_15s = _15s.index(max_f_15s)
        f_15s = [ max_f_15s - i  >  10 for i in _15s[:m_pos_f_15s]]
        f_15s_count = f_15s.count(True)
        
    # print("%s %4d %4d %4d\n" % (id, dur, max_f_15s, f_15s_count))

    
    # rs = dur < 300 and dur > 30 and sum(curr[3])
    rs = f_15s_count > 2
    return rs
    

def main(argv):
    data_dirs = []
    data_files = []
    
    data_dirs.append('/home/duydb2/proj/zalo/stats_log_data/23_12_2015')
    data_files = [file[0] for file in get_data_files(data_dirs,'.gz').values()]
    call_ids = fileter_call_ids(data_files)
    for i,item in enumerate(call_ids):
        print(i, item)
    import test_runner as app
    # app.show(call_ids)
    

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))