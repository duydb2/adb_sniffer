#!/usr/bin/env python

import os
import sys

local_path = os.path.dirname(os.path.abspath(__file__))

import re
import json

def get_value(json_str, key):
    try:
        objson = json.loads(json_str)
        result = objson
        keys = key.split('%')
        for k in keys:
            if k:
                rk = re.search(r'\{(.*)|\[(.*)',k)
                if rk:
                    obj_key, list_key = rk.groups()
                    if obj_key:
                        result = result.get(obj_key, {})
                    if list_key:
                        result = result[int(list_key)]
                if not result:
                    break
            
        if not result:
            return 0
        return float(result)
    except Exception:
        return 0
            
def map_json_to_csv(json_str, maps):
    rs = []
    for i,key in enumerate(maps):
        rs.append(get_value(json_str,key))
    return rs

def load_json_to_csv(path, json_csv_map):
    data = []
    with open(path, 'r') as f:
        for line in f:
            results = re.compile(r'(\{.*\}|\[.*\])', re.IGNORECASE | re.MULTILINE).search(line)
            if results:
                results = results.group(0)
                data.append(map_json_to_csv(results, json_csv_map))
    return data
    
def load_json_data(path, json_map):
    data = {}
    with open(path, 'r') as f:
        for line in f:
            results = re.compile(r'(\{.*\}|\[.*\])', re.IGNORECASE | re.MULTILINE).search(line)
            if results:
                results = results.group(0)
                for key in json_map:
                    line = data.get(key)
                    if not line:
                        line = []
                    line.append(get_value(results, key))
                    data.update({key:line})
    return data

def main(argv):
    print load_json_data('/home/duydb2/proj/zalo/adb_sniffer/new_stats/test/data/117012862_100515953_1486936188_1446743508624',     
        [
        '[1%[1%[3%[1', # 0 - sent package
        '[1%[1%[2%[1', # 1 - received package
        '[1%[1%[3%[0', # 2 - sent byte
        '[1%[1%[2%[0', # 3 - Received byte
        '[1%[1%[5%[0', # 4 - RTT
        '[1%[1%[5%[1', # 5 - Loss rate
        '[1%[1%[0%[0', # 6 - RTT avg
        '[1%[1%[0%[1', # 7 - RTT last
        '[1%[1%[4%[0', # 8 - loss package rate
        '[1%[1%[1%[3', # 9 - FEC sent rate
        '[1%[1%[1%[4', # 10 - FEC received rate
        '[1%[1%[1%[5', # 11 - FEC recover rate
        '[1%[1%[1%[7', # 12 - FEC real rate
        '[1%[1%[4%[1', # 13 - Loss package total
        '[1%[1%[1%[0', # 14 - FEC sent total
        '[1%[1%[1%[1', # 15 - FEC received total
        '[1%[1%[1%[2', # 16 - FEC recover total
        '[1%[1%[1%[6', # 17 - FEC real total
        '[1%[1%[5%[2', # 18 - echo point
        '[1%[1%[7%[0', # 19 - quality point
        '[1%[1%[8%[0', # 20 - Delay jitter
        '[1%[1%[8%[1', # 21 - Delay network
        '[1%[1%[9%[1', # 22 - Device current upload rate
        '[1%[1%[9%[0', # 23 - Device current download rate
    ])['[1%[1%[3%[1']
    
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
                