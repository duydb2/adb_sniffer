#!/usr/bin/env python

import os
import sys

local_path = os.path.dirname(os.path.abspath(__file__))

import matplotlib.pyplot as plt
import matplotlib.animation as manim
import matplotlib
import data
import config

plts_instance = []
plots_data = {}

def create_subplot(fig, plts_num, order, chartid):
    global plots_data
    lines = []
    col = 1 
    row = plts_num // col + 1
    splt = fig.add_subplot(row, col, order)
    splt.grid()
    splt.set_title('%s'%config.plots2[chartid]['title'])
    lineids = config.plots2[chartid]['lines']
    for id in lineids:
        line, = splt.plot([], label='%s' % config.lines_defines[id]['label'])
        lines.append({
                'ax': line, 
                'key':config.lines_defines[id]['key'],
                'label': config.lines_defines[id]['label'],
            })
   
    splt.legend(loc='upper left', ncol=1, fancybox=True, shadow=False, fontsize = 'x-small')
    def update(i):
        for line in lines:
            key = line['key']
            ax = line['ax']
            x = plots_data['[1%[0']
            line_data = plots_data[key]
            ax.set_data(x,line_data)
            print 'create_subplot', x
            
        splt.relim()
        splt.autoscale_view(True,True,True)
        # fig.canvas.draw()
        
    return update

def show(path):
    global plts_instance, plots_data
    json_csv_map_config = [value['key'] for key, value in config.lines_defines.items()]
    json_csv_map_config.append('[1%[0')
    # get data
    plots_data = data.load_json_data(path,json_csv_map_config)
    # add subplot
    plt.close('all')
    fig = plt.figure(figsize=(20,60),dpi=75)
    fig.subplots_adjust(left=0.04, bottom=0.05, right=0.98, top=0.95, wspace=0.15, hspace=0.15)
    plts_instance = [create_subplot(fig, len(config.ids_win2), order + 1, chartid) for order, chartid in enumerate(config.ids_win2)]
    # show
    return fig, plt

def update(i):
    global plts_instance
    for plt in plts_instance:
        plt(i)
    
def main2(argv):
    json_csv_map_config = [value['key'] for key, value in config.lines_defines.items()]
    json_csv_map_config.append('[1%[0')
    lines = data.load_json_data('/home/duydb2/proj/zalo/adb_sniffer/new_stats/test/data/117012862_100515953_1486936188_1446743508624',json_csv_map_config)
    print lines['[1%[1%[3%[1']

def main(argv):
    fig, plt = show('/home/duydb2/Downloads/data/119529606_131876033_1489072847_1446745740962')
    ani1 = manim.FuncAnimation(fig,update, interval=500)
    plt.show()

    #trendline(xx, yy, xtick_pos_and_labels=[[0,6,12,18,24],['2000','2006','2012','2018','2024']], fill=False)

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))