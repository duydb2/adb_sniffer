call_log_file = 'D:/vm/webrtc-pjproject/adb_sniffer/realtime_stats/call2.txt'
log_file = None

font = {'family' : 'serif',
        'color'  : 'darkred',
        'weight' : 'normal',
        'size'   : 16,
        }

id_5 = 0

### define graph 
plots = {
       20:[id_5,
         ["%ds", "%d pkg"],
         [['[1%[1%[3%[1','Sent pkg'], ['[1%[1%[2%[1','Received pkg']],
         'Sent/Received package'],                        # Sent/Received
       21:[id_5,
         ["%ds", "%.1f kbps"],
         [['[1%[1%[3%[0','Sent byte'], ['[1%[1%[2%[0','Received byte']],
         'Sent/Received byte'],                           # Sent/Received
       22:[id_5,
         ["%ds", "%.1f ms"],
         [['[1%[1%[5%[0','RTT']],
         'Echo RTT'],                                     # Echo RTT
       23:[id_5,
         ["%ds", "%.1f"],
         [['[1%[1%[5%[1','Loss rate']],
         'Echo Loss rate'],                               # Echo Loss rate
       24:[id_5,
         ["%ds", "%.1fms"],
         [['[1%[1%[0%[0','avg '], ['[1%[1%[0%[1','last']],
         'RTT'],                                          # RTT avg + last
       25:[id_5,
         ["%ds", "%d pkg"],
         [['[1%[1%[4%[0','rate']],
         'Loss pkg rate'],                                # Loss pkg rate
       26:[id_5,
         ["%ds", "%d pkg"],
         [['[1%[1%[1%[3','sent'], ['[1%[1%[1%[4','received'], ['[1%[1%[1%[5','recover'], ['[1%[1%[1%[7','real']],
         'FEC rate'],                                     # FEC rate
       27:[id_5,
         ["%ds", "%d pkg"],
         [['[1%[1%[4%[1','rate']],
         'Loss pkg total'],                               # Loss pkg total
       28:[id_5,
         ["%ds", "%d pkg"],
         [['[1%[1%[1%[0','sent'], ['[1%[1%[1%[1','received'], ['[1%[1%[1%[2','recover'], ['[1%[1%[1%[6','real']],
         'FEC total'],                                    # FEC total
       29:[id_5,
         ['%ds','%d star'],
         [['[1%[1%[5%[2','Echo'], ['[1%[1%[7%[0','Quality']],
         'Point'],
       30:[id_5,
         ['%d','%dpkg'],
         [['[1%[1%[8%[0','Jitter'],['[1%[1%[8%[1','Network']],
         'Delay'],
       31:[id_5,
         ['%d', '%.2fkbps'],
         [['[1%[1%[9%[1','Upload'],['[1%[1%[9%[0','Download']],
         'Device current network rate']
}

ids_win1 = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31] # for other window


lines_defines = {
    0:{ 'key': '[1%[1%[3%[1', 'label': 'Sent package                 ' },  # 0 - sent package
    1:{ 'key': '[1%[1%[2%[1', 'label': 'Received package             ' },  # 1 - received package
    2:{ 'key': '[1%[1%[3%[0', 'label': 'Sent byte                    ' },  # 2 - sent byte
    3:{ 'key': '[1%[1%[2%[0', 'label': 'Received byte                ' },  # 3 - Received byte
    4:{ 'key': '[1%[1%[5%[0', 'label': 'RTT                          ' },  # 4 - Echo RTT
    5:{ 'key': '[1%[1%[5%[1', 'label': 'Loss rate                    ' },  # 5 - Loss rate
    6:{ 'key': '[1%[1%[0%[0', 'label': 'RTT avg                      ' },  # 6 - RTT avg
    7:{ 'key': '[1%[1%[0%[1', 'label': 'RTT last                     ' },  # 7 - RTT last
    8:{ 'key': '[1%[1%[4%[0', 'label': 'loss package rate            ' },  # 8 - loss package rate
    9:{ 'key': '[1%[1%[1%[3', 'label': 'FEC sent rate                ' },  # 9 - FEC sent rate
    10:{ 'key': '[1%[1%[1%[4', 'label': 'FEC received rate           ' },  # 10 - FEC received rate
    11:{ 'key': '[1%[1%[1%[5', 'label': 'FEC recover rate            ' },  # 11 - FEC recover rate
    12:{ 'key': '[1%[1%[1%[7', 'label': 'FEC real rate               ' },  # 12 - FEC real rate
    13:{ 'key': '[1%[1%[4%[1', 'label': 'Loss package total          ' },  # 13 - Loss package total
    14:{ 'key': '[1%[1%[1%[0', 'label': 'FEC sent total              ' },  # 14 - FEC sent total
    15:{ 'key': '[1%[1%[1%[1', 'label': 'FEC received total          ' },  # 15 - FEC received total
    16:{ 'key': '[1%[1%[1%[2', 'label': 'FEC recover total           ' },  # 16 - FEC recover total
    17:{ 'key': '[1%[1%[1%[6', 'label': 'FEC real total              ' },  # 17 - FEC real total
    18:{ 'key': '[1%[1%[5%[2', 'label': 'echo point                  ' },  # 18 - echo point
    19:{ 'key': '[1%[1%[7%[0', 'label': 'quality point               ' },  # 19 - quality point
    20:{ 'key': '[1%[1%[8%[0', 'label': 'Delay jitter                ' },  # 20 - Delay jitter
    21:{ 'key': '[1%[1%[8%[1', 'label': 'Delay network               ' },  # 21 - Delay network
    22:{ 'key': '[1%[1%[9%[1', 'label': 'Device current upload rate  ' },  # 22 - Device current upload rate
    23:{ 'key': '[1%[1%[9%[0', 'label': 'Device current download rate' },  # 23 - Device current download rate
}

### define graph 
plots2 = {
            20:{  # Sent/Received package
                'x_unit' : '%ds',
                'y_unit' : '%d pkg',
                'lines' : [0,1],
                'title': 'Sent/Received package',
            },                       
            21:{  # Sent/Received byte         
                'x_unit' : '%ds',
                'y_unit' : '%.1f kbps',
                'lines' : [2,3],
                'title': 'Sent/Received byte',
            },                          
            22:{  # Echo RTT
                'x_unit' : '%ds',
                'y_unit' : '%.1f ms',
                'lines' : [4],
                'title': 'Echo RTT',
            },
            23:{  # Echo Loss rate
                'x_unit' : '%ds',
                'y_unit' : '%.1f',
                'lines' : [5],
                'title': 'Loss rate',
            },
            24:{  # Echo Loss rate
                'x_unit' : '%ds',
                'y_unit' : '%.1fms',
                'lines' : [6,7],
                'title': 'RTT avg + last',
            },
            25:{  # Echo pkg rate
                'x_unit' : '%ds',
                'y_unit' : '%d pkg',
                'lines' : [8],
                'title': 'Loss pkg rate',
            }
}

ids_win2 = [20, 21, 22, 23, 24, 25]

##ids_win1 = [3,5,4,6,7,10,1,2,8]

## 3 - Received rate (byte)
## 5 - Received Pkg rate
## 4 - Sent rate (byte)
## 6 - Sent Pkg rate
## 7 - Loss Pkg rate,
## 10 - Loss pkg total (Expected package)
## 1 - RTT Average, Last
## 2 - FEC: Rate
## 9 - FEC: Total
## 8 - Echo: RTT, Loss, Point (Ngoc - New)
