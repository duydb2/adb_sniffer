#!/usr/bin/env python
# http://shortrecipes.blogspot.com/2014/05/python-3-and-tkinter-scroll-canvas-with.html
# https://bytes.com/topic/python/answers/435227-how-do-i-bind-mousewheel-scrollbar
# https://www.daniweb.com/programming/software-development/code/217059/using-the-mouse-wheel-with-tkinter-python
# https://mail.python.org/pipermail/tkinter-discuss/2006-June/000808.html
# http://code.activestate.com/recipes/578894-mousewheel-multiplatform-tkinter/
# http://stackoverflow.com/questions/17355902/python-tkinter-binding-mousewheel-to-scrollbar
import os
import sys

local_path = os.path.dirname(os.path.abspath(__file__))

import matplotlib.animation as animation
from matplotlib import style
style.use("ggplot")

sys.path.append(local_path)

import py as stats
from Tkinter import Tk, Frame, Canvas, Scrollbar 
from Tkconstants import NSEW, HORIZONTAL, EW, NS, ALL 

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg 


def getScrollingCanvas(frame): 
     """ 
         Adds a new canvas with scroll bars to the argument frame 
         NB: uses grid layout 
         @return: the newly created canvas 
     """ 

     frame.grid(sticky=NSEW) 
     frame.rowconfigure(0, weight=1) 
     frame.columnconfigure(0, weight=1) 

     canvas = Canvas(frame) 
     canvas.grid(row=0, column=0, sticky=NSEW) 

     xScrollbar = Scrollbar(frame, orient=HORIZONTAL) 
     yScrollbar = Scrollbar(frame) 

     xScrollbar.grid(row=1, column=0, sticky=EW) 
     yScrollbar.grid(row=0, column=1, sticky=NS) 

     canvas.config(xscrollcommand=xScrollbar.set) 
     xScrollbar.config(command=canvas.xview) 
     canvas.config(yscrollcommand=yScrollbar.set) 
     yScrollbar.config(command=canvas.yview) 

     return canvas 

if __name__ == "__main__": 

     root = Tk() 
     root.rowconfigure(0, weight=1) 
     root.columnconfigure(0, weight=1) 

     frame = Frame(root) 

     scrollC = getScrollingCanvas(frame) 

     fig,plt = stats.graph.show('/home/duydb2/proj/zalo/adb_sniffer/new_stats/test/data/117012862_100515953_1486936188_1446743508624')

     mplCanvas = FigureCanvasTkAgg(fig, scrollC) 
     canvas = mplCanvas.get_tk_widget() 
     canvas.grid(sticky=NSEW) 

     scrollC.create_window(0, 0, window=canvas) 
     scrollC.config(scrollregion=scrollC.bbox(ALL)) 

     root.mainloop() 
